import { Component, OnInit } from '@angular/core';
import { Employee } from '../Employee';

@Component({
  selector: 'app-view-emp',
  templateUrl: './view-emp.component.html',
  styleUrls: ['./view-emp.component.css']
})
export class ViewEmpComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  emp:Employee={
    id:3,  
    name:"john",   
    salary:10000, 
    permanent:"Yes",
    department:{dept_id:1,dept_name:"payroll"},
    skill:[    
    {skill_id:100,skill_name:"HTML"},
    {skill_id:101,skill_name:"CSS"},
    {skill_id:102,skill_name:"JavaScript"}],
    dob:new Date('12/31/2000'),
  };   
}
               